from django import forms
from samples.models import Sample, Key


# class SampleCreateForm(forms.ModelForm):
#     class Meta:
#         model = Sample



class KeyForm(forms.ModelForm):
    class Meta:
        model = Key
        fields = ['name']
