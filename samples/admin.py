from django.contrib import admin
from samples.models import Sample, Key, Artist

class SampleAdmin(Sample):
    pass

class KeyAdmin(Key):
    pass

class ArtistAdmin(Artist):
    pass

admin.site.register(Sample)
admin.site.register(Key)
admin.site.register(Artist)

# Register your models here.
