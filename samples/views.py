from re import template
from django.shortcuts import render
from samples.models import Home, Sample, Key
from samples.forms import KeyForm
from django.views.generic.edit import CreateView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView


class SampleListView(ListView):
    model = Sample
    template_name = "samples/list.html"


class CreateKeyView(CreateView):
    model = Key
    form_class = KeyForm
    template_name = 'samples/template.html'
    success_url = 'samples/success.html'

def homepage(request):
    homepage = Home.objects.all()
    context = {"homepage": homepage}

    return render(request, "samples/homepage.html", context)


def show_key(request, key):
    key = Sample.objects.filter(key=key)
    context = {
        "key": key,
    }
    return render(request, "samples/key.html", context)

