from django.urls import path
from samples.views import SampleListView, homepage, show_key

urlpatterns = [
    path("homepage/", homepage, name="homepage"),
    path("", SampleListView.as_view(), name="list_samples"),
    path("<str:key>/keys/", show_key, name="show_keys")
    ]
