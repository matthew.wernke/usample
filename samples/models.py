from django.db import models
from django.conf import settings

KEY_CHOICES = (
    ('c', 'C'),
    ('d', 'D'),
    ('e', 'E'),
    ('f', 'F'),
    ('g', 'G'),
    ('a', 'A'),
    ('b', 'B'),
)

class Key(models.Model):
    name = models.CharField(max_length=2, choices=KEY_CHOICES, default="c")

    def __str__(self):
        return self.name

class Sample(models.Model):
    name = models.CharField(max_length=100)
    audio = models.FileField(upload_to='audio/', blank=True)
    type = models.TextField()
    key = models.ForeignKey(Key, related_name="samples", on_delete=models.PROTECT)
    artists = models.ManyToManyField("Artist", related_name="samples")

    def __str__(self):
        return self.name

class Artist(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Home(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

