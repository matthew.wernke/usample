1. Clone usample
2. Create Virtual Environment
3. Activate
4. Install / upgrade pip
5. Install Django
6. createsuper user
7. [SETUP]
8. Create sampleproject project
9. Create samples app
10. Create accounts app
11. [samples-app]
12. Create Sample model
13. * Class Sample(models.Model)
    * name = models.CharField(max_length=100)
    * audio = models. FileField(upload_to='audio/', blank=True) 
    * type = models.TextField()
    * key = models.ForeignKey(USER_MODEL, related_name="samples"
    * Artists = models.ManyToManyField("Artist", related_name="samples")
14. Convert to __str__:
15. Register Sample in admin
16. [Sample-Views.py]
17. * def list_samples(request):
    * samples = Sample.objects.filter(members=request.user)
    * context = {"samples": samples}

    * return render(request, "samples/list.html", context)
18. Register view path "" name= list_samples -> samples.urls.py
19. In sampleproject include URL "samples/"
20. Create HTML for ListView
21. Created a forms.py